import json

from flask import Flask, request

import source


app = Flask(__name__)

PAST = {
	"open":"opened",
	"close":"closed",
	"reopen":"reopened"
}


@app.route('/',methods=['POST'])
def foo():
	global PAST
	try:
		notif = request.data.decode('utf-8')
	except:
		notif = request.data
	notif = json.loads(notif)
	kind = notif["object_kind"]
	event_type = notif["event_type"]
	user = notif["user"]["name"]
	repo = notif["project"]["name"]
	url = notif["object_attributes"]["url"]
	if kind == "issue":
		issue = notif["object_attributes"]["title"]
		action = PAST[notif["object_attributes"]["action"]]
	elif kind == "note":
		issue = notif["issue"]["title"]
		action = "commented on"
	else:
		return "OK"
	ret = "[\x0310{repo}\x03] \x033{user}\x03 {action} {issue}: {url}".format(
		repo=repo,
		user=user,
		action=action,
		issue=issue,
		url=url
	)
	push_update = app.config['push_update']
	push_update(repo, ret)
	return "OK"


class GitlabIssue(source.Source):

	def __init__(self):
		source.Source.__init__(self, 'gitlabissue', 'gitlab.crans.org')

	def loop(self, *args, **kwargs):
		app.config['push_update'] = self.push_update
		app.run(host='138.231.136.1' , port=5001)
