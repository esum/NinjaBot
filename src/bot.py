import irc.bot

import config


class Bot(irc.bot.SingleServerIRCBot):

    def __init__(self, nickname, channel="#bot", server="irc.crans.org", port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.nick = nickname
        self.channel = channel
        self.operators = config.operators

    def on_nicknameinuse(self, conn, e):
        conn.nick(conn.get_nickname() + "_")
        self.nick += "_"

    def on_welcome(self, conn, e):
        conn.join(self.channel)
        self.on_welcome_ext(conn, e)

    def on_privmsg(self, conn, e):
        if e.source.nick in self.operators:
            return self.do_command(conn, e.arguments[0].split(' '), self.operators[e.source.nick], e.source.nick)
        return self.do_command(conn, e.arguments[0].split(' '), 0, e.source.nick)

    def do_command(self, conn, command, level, source):
        if command[0].casefold() == "op":
            if len(command) == 2:
                dst_level = 100
            else:
                try:
                    dst_level = int(command[2])
                except:
                    dst_level = 0
            if 0 <= dst_level < level:
                self.operators[command[1]] = dst_level
        elif command[0].casefold() == "join" and level >= 100:
            for channel in command[1:]:
                conn.join(channel)
        elif command[0].casefold() == "leave" and level >= 100:
            for channel in command[1:]:
                conn.part(channel, "Ce n'est qu'un au revoir")
        elif command[0].casefold() == "nick" and level >= 100 and len(command) >= 2:
            self.nick = command[1]
            conn.nick(self.nick)
        else:
            self.do_command_ext(conn, command, level, source)
