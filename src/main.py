import socket

import config

# Sources
import source
import github
import wiki
import news
import phabricator
import prometheus
import publinet
import ml
import gitlabissue


MAIL_SOURCES = [wiki.Wiki(), phabricator.Phabricator(), ml.Ml(), github.Github()]
SOURCES = [source.MailSources(MAIL_SOURCES), news.News(), prometheus.Prometheus(), publinet.Publinet(), gitlabissue.GitlabIssue()]

if __name__ == "__main__":
	with open(config.socket_path, 'w') as fifo:
		fifo.write('SOURCES ' + ','.join([source.name for source in SOURCES + MAIL_SOURCES if source.name]))
	[ source.thread.join() for source in SOURCES ]
