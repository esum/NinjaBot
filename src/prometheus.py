import json

from flask import Flask, request

import source


app = Flask(__name__)

@app.route('/',methods=['POST'])
def foo():
	try:
		notif = request.data.decode('utf-8')
	except:
		notif = request.data
	print(notif)
	data = json.loads(notif)
	push_update = app.config['push_update']
	#push_update(data['commonLabels']['instance'], '\x0310{alertname}\x03 \x033{instance}\x03 {job}\x03'.format(**data['commonLabels']))
	for alert in data['alerts']:
		# Retrieve metadata
		status = alert.get('status', 'firing')
		labels = alert.get('labels', {})
		instance = labels.get('instance', '')
		alertname = labels.get('alertname', '')
		severity = labels.get('severity', 'unknown')
		annotations = alert.get('annotations', {})
		summary = annotations.get('summary', '')
		description = annotations.get('description', '')

		if status == 'resolved':
			message = '\x0310{alertname}\x03 \x033resolved\x0315 {summary}\x03'.format(alertname=alertname, severity=severity, summary=summary)
			push_update(instance, message)
		else:
			message = '\x0310{alertname}\x03 \x034{severity}\x03 {summary}\x03'.format(alertname=alertname, severity=severity, summary=summary)
			push_update(instance, message)
			if description:
				push_update(instance, description)

	return "OK"


class Prometheus(source.Source):

	def __init__(self):
		source.Source.__init__(self, 'prometheus', 'prometheus.adm.crans.org')

	def loop(self, *args, **kwargs):
		app.config['push_update'] = self.push_update
		app.run(host='10.231.136.1' , port=5000)
