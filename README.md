NinjaBot
========

NinjaBot est un bot irc permettant d'abonner des canaux et des utilisateurs irc à des nofications provenants de diverses sources (mail, serveur de news…)

Par défaut il y a 3 sources : `wiki`, `news` et `phabricator`

Install
-------

Pour installer NinjaBot créez un `config.py` et un `secrets.py` dans `src/`, vous pouvez pour celà vous inspirer des fichiers d'exemples.

Créez ensuite un fichier `subs.py` dans `src/` avec le contenu :
```
subs = {}
``` 

Running
-------

Pour démarrer NinjaBot lancez tout d'abord `ninjabot.py` :
```
cd src/
python3 ninjabot.py &
```

Ensuite patientez quelque instants, un fichier fifo devrait être créé (`ninja.socket` si vous avez gardé la valeur dans la configuration d'exemple), puis lancez `main.py` :
```
python3 main.py
```

Celà devrait suffire, les sources devraient se charger et leurs threads devraient se lancer.

Commandes IRC
-------------

Pour vous abonner vous pouvez ensuite envoyer des commandes en message privé à NinjaBot sur votre serveur irc.

À tout moment vous pouvez utiliser la commande `help` pour obtenir une aide rapide sur les différentes commandes.

Voici la liste des principales commandes :

 - `sub source dest regex`
   * `source` : peut être l'une des sources chargées.
   * `dest` : peut un canal irc ou votre nick irc.
   * `regex` : une expression régulière au format python (cf. https://docs.python.org/3/library/re.html) qui peut contenir des espaces.
   * Abonne un canal ou vous-même (`dest`) aux notifications provenant de `source` si la «page» de la notification correspond à l'expression régulière `regex` (référez vous à la documentation de la source pour le format d'une «page»)

Source
------

### Wiki

Notifications mail provenant de https://wiki.crans.org.

Une «page» est une page wiki.

### News

Notifications mail provenant de news.crans.org.

Une «page» est un groupe de news.

### Phabricator

Notifications mail provenant de https://phabricator.crans.org.

Une «page» est une tâche au format `Txxx` si cette tâche n'est pas dans un groupe, sinon elle est au format `group/Txxx` où `group` est en minuscules et les éventuelles espaces qu'il contient sont remplacés par des `_` (par exemple «Groupe Sympa» devient `groupe_sympa`).
